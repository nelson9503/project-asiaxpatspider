
from AsiaXPatSpider import general_tools


def extract(lines: list) -> dict:
    """
    Extract Content Page.
    """
    result = {}

    for i in range(len(lines)):

        line = lines[i]

        # title and subtitle - keywords are For Rent or Direct Owner
        if line == "For Rent" or line == "Direct Owner":
            if not "title" in result and not "subtitle" in result: # safe lock
                result["title"] = lines[i+4]
                result["price"] = lines[i+6]
                result["subtitle"] = lines[i+8]

        # Description  
        elif line == "Description":
            i2 = i + 1 # we scan from next line
            desc = ""
            while not "Show full" in lines[i2]: # break while find Show full
                if not lines[i2] == "":
                    desc += lines[i2] + "\n"
                i2 += 1
        
        # Last Update
        elif "Update on " in line:
            x = line.replace("Update on ", "")
            x = general_tools.removeAbnormalSpaces(x)
            result["lastUpdate"] = x
        
        # Address
        elif "Address: " in line:
            i2 = i
            joinText = ""
            # we join the text until next line break
            while not lines[i2] == "":
                joinText += lines[i2]
                i2 += 1
            address = joinText.replace("Address: ", "").split("[")[0]
            address = general_tools.removeAbnormalSpaces(address)
            result["address"] = address
        
        # Bedrooms
        elif "Bedrooms: " in line:
            x = line.split(":")[1]
            x = general_tools.removeAbnormalSpaces(x)
            result["bedrooms"] = x
        
        # Bathrooms
        elif "Bathrooms: " in line:
            x = line.split(":")[1]
            x = general_tools.removeAbnormalSpaces(x)
            result["bathrooms"] = x
        
        # Building Type
        elif "Building Type: " in line or "Property Type: " in line:
            x = line.split(":")[1]
            x = general_tools.removeAbnormalSpaces(x)
            result["buildingType"] = x
        
        # Property Layout
        elif "Property Layout: " in line:
            x = line.split(":")[1]
            x = general_tools.removeAbnormalSpaces(x)
            result["propertyLayout"] = x
        
        # Floor Zone
        elif "Floor Zone: " in line:
            x = line.split(":")[1]
            x = general_tools.removeAbnormalSpaces(x)
            result["floorZone"] = x
        
        # Gross Area
        elif "Gross Area: " in line:
            x = line.split(":")[1]
            x = x.split("ft")[0]
            x = general_tools.removeAbnormalSpaces(x)
            result["grossArea"] = x
        
        # Saleable Area
        elif "Saleable Area: " in line:
            x = line.split(":")[1]
            x = x.split("ft")[0]
            x = general_tools.removeAbnormalSpaces(x)
            result["saleArea"] = x
        
        # Facility
        elif line == "Facilities":
            i2 = i + 1
            facilities = []
            while not lines[i2] == "* * *": # break when find * * *
                if not lines[i2] == "":
                    facilities.append(lines[i2])
                i2 += 1
            result["facilities"] = facilities
        
        # Rooms
        elif line == "Rooms":
            i2 = i + 1
            rooms = []
            while not lines[i2] == "* * *": # break when find * * *
                if not lines[i2] == "":
                    rooms.append(lines[i2])
                i2 += 1
            result["rooms"] = rooms
        
        # Views
        elif line == "Views":
            i2 = i + 1
            views = []
            while not lines[i2] == "* * *": # break when find * * *
                if not lines[i2] == "":
                    views.append(lines[i2])
                i2 += 1
            result["views"] = views
        
        # Contact Person
        elif line == "Contact Details" and not "contactPerson" in result:
            result["contactPerson"] = lines[i+2]
        
        # Contact Phone
        elif "(tel:" in line and not "contactPhone" in result:
            x = line.split("(tel:")[1]
            x = x.split(")")[0]
            result["contactPhone"] = general_tools.removeAbnormalSpaces(x)
        
    return result