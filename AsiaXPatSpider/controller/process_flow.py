import os
import datetime
import xlsxwriter as xw
from AsiaXPatSpider.controller.user_ui import UI
from AsiaXPatSpider import cover_extractor, general_tools
from AsiaXPatSpider import content_extractor


def main_flow():

    # web host
    webhost = "https://hongkong.asiaxpat.com"

    # output file
    date = datetime.datetime.today()
    folder = "./asiaxpat-{:04d}{:02d}{:02d}".format(
        date.year, date.month, date.day)
    if not os.path.exists(folder):
        os.mkdir(folder)
    if not os.path.exists(folder+"/image"):
        os.mkdir(folder+"/image")

    # create ui
    ui = UI()
    ui.report()

    # Direct Owner Apartments for Rent #

    # phase 1
    page = 1
    properties = {}
    while True:
        ui.directOwnerRentPhase1 = "collecting property information...\tpage:{}".format(
            page)
        ui.report()
        url = "https://hongkong.asiaxpat.com/property/direct-owner-apartments-for-rent/{}/".format(
            page)
        lines = general_tools.get_web_text(url)
        j = cover_extractor.extract(lines)
        if len(j) == 0:
            break
        for key in j:
            properties[key] = j[key]
        page += 1
    ui.directOwnerRentPhase1 = "Done."
    ui.report()

    # phase 2
    step = 0
    dellist = []
    for property in properties:
        step += 1
        ui.directOwnerRentPhase2 = "collecting details of properties...\t{}/{}".format(
            step, len(properties))
        ui.report()
        info = properties[property]
        url = webhost + info["url"]
        while True:
            lines = general_tools.get_web_text(url)
            j = content_extractor.extract(lines)
            if len(j) > 0:
                break
        for key in j:
            info[key] = j[key]
        if info["subtitle"] == "":
            dellist.append(property)
            continue
        properties[property] = info
    for property in dellist:
        del properties[property]
    ui.directOwnerRentPhase2 = "Done."
    ui.report()

    # phase 3
    step = 0
    for property in properties:
        step += 1
        image_urls = properties[property]["image_urls"]
        id = properties[property]["id"]
        image = 0
        for image_url in image_urls:
            image += 1
            ui.directOwnerRentPhase3 = "downloading images of properties... | {}/{} | {}/{}".format(
                step, len(properties), image, len(image_urls))
            ui.report()
            url = webhost + image_url
            general_tools.get_web_photo(
                url, folder+"/image/"+id+"-"+str(image)+".jpg")
    ui.directOwnerRentPhase3 = "Done."
    ui.report()

    # file export
    wb = xw.Workbook(folder+"/direct_owner_appartments_for_rent.xlsx")
    sh = wb.add_worksheet("output")
    headers = {
        "id": "A1",
        "title": "B1",
        "subtitle": "C1",
        "price": "D1",
        "grossArea": "E1",
        "saleArea": "F1",
        "address": "G1",
        "bedrooms": "H1",
        "bathrooms": "I1",
        "buildingType": "J1",
        "propertyLayout": "K1",
        "floorZone": "L1",
        "contactPerson": "M1",
        "contactPhone": "N1",
        "facilities": "O1",
        "rooms": "P1",
        "views": "Q1"
    }
    for header in headers:
        sh.write(headers[header], header)
    row = 2
    for property in properties:
        info = properties[property]
        try:
            sh.write("A"+str(row), info["id"])
        except KeyError:
            pass
        try:
            sh.write("B"+str(row), info["title"])
        except KeyError:
            pass
        try:
            sh.write("C"+str(row), info["subtitle"])
        except KeyError:
            pass
        try:
            sh.write("D"+str(row), info["price"])
        except KeyError:
            pass
        try:
            sh.write("E"+str(row), info["grossArea"])
        except KeyError:
            pass
        try:
            sh.write("F"+str(row), info["saleArea"])
        except KeyError:
            pass
        try:
            sh.write("G"+str(row), info["address"])
        except KeyError:
            pass
        try:
            sh.write("H"+str(row), info["bedrooms"])
        except KeyError:
            pass
        try:
            sh.write("I"+str(row), info["bathrooms"])
        except KeyError:
            pass
        try:
            sh.write("J"+str(row), info["buildingType"])
        except KeyError:
            pass
        try:
            sh.write("K"+str(row), info["propertyLayout"])
        except KeyError:
            pass
        try:
            sh.write("L"+str(row), info["floorZone"])
        except KeyError:
            pass
        try:
            sh.write("M"+str(row), info["contactPerson"])
        except KeyError:
            pass
        try:
            sh.write("N"+str(row), info["contactPhone"])
        except KeyError:
            pass
        if "facilities" in info:
            x = ""
            for i in range(len(info["facilities"])):
                x += info["facilities"][i] + ","
            sh.write("O"+str(row), x)
        if "rooms" in info:
            x = ""
            for i in range(len(info["rooms"])):
                x += info["rooms"][i] + ","
            sh.write("P"+str(row), x)
        if "views" in info:
            x = ""
            for i in range(len(info["views"])):
                x += info["views"][i] + ","
            sh.write("Q"+str(row), x)
        row += 1
    wb.close()

    # Direct Owener Apartments for Sale #

    # phase 1
    page = 1
    properties = {}
    while True:
        ui.directOwnerSalePhase1 = "collecting property information...\tpage:{}".format(
            page)
        ui.report()
        url = "https://hongkong.asiaxpat.com/property/direct-owner-apartments-for-sale/{}/".format(
            page)
        lines = general_tools.get_web_text(url)
        j = cover_extractor.extract(lines)
        if len(j) == 0:
            break
        for key in j:
            properties[key] = j[key]
        page += 1
    ui.directOwnerSalePhase1 = "Done."
    ui.report()

    # phase 2
    step = 0
    dellist = []
    for property in properties:
        step += 1
        ui.directOwnerSalePhase2 = "collecting details of properties...\t{}/{}".format(
            step, len(properties))
        ui.report()
        info = properties[property]
        url = webhost + info["url"]
        while True:
            lines = general_tools.get_web_text(url)
            j = content_extractor.extract(lines)
            if len(j) > 0:
                break
        for key in j:
            info[key] = j[key]
        if info["subtitle"] == "":
            dellist.append(property)
            continue
        properties[property] = info
    for property in dellist:
        del properties[property]
    ui.directOwnerSalePhase2 = "Done."
    ui.report()

    # phase 3
    step = 0
    for property in properties:
        step += 1
        image_urls = properties[property]["image_urls"]
        id = properties[property]["id"]
        image = 0
        for image_url in image_urls:
            image += 1
            ui.directOwnerSalePhase3 = "downloading images of properties... | {}/{} | {}/{}".format(
                step, len(properties), image, len(image_urls))
            ui.report()
            url = webhost + image_url
            general_tools.get_web_photo(
                url, folder+"/image/"+id+"-"+str(image)+".jpg")
    ui.directOwnerSalePhase3 = "Done."
    ui.report()

    # file export
    wb = xw.Workbook(folder+"/direct_owner_appartments_for_sale.xlsx")
    sh = wb.add_worksheet("output")
    headers = {
        "id": "A1",
        "title": "B1",
        "subtitle": "C1",
        "price": "D1",
        "grossArea": "E1",
        "saleArea": "F1",
        "address": "G1",
        "bedrooms": "H1",
        "bathrooms": "I1",
        "buildingType": "J1",
        "propertyLayout": "K1",
        "floorZone": "L1",
        "contactPerson": "M1",
        "contactPhone": "N1",
        "facilities": "O1",
        "rooms": "P1",
        "views": "Q1"
    }
    for header in headers:
        sh.write(headers[header], header)
    row = 2
    for property in properties:
        info = properties[property]
        try:
            sh.write("A"+str(row), info["id"])
        except KeyError:
            pass
        try:
            sh.write("B"+str(row), info["title"])
        except KeyError:
            pass
        try:
            sh.write("C"+str(row), info["subtitle"])
        except KeyError:
            pass
        try:
            sh.write("D"+str(row), info["price"])
        except KeyError:
            pass
        try:
            sh.write("E"+str(row), info["grossArea"])
        except KeyError:
            pass
        try:
            sh.write("F"+str(row), info["saleArea"])
        except KeyError:
            pass
        try:
            sh.write("G"+str(row), info["address"])
        except KeyError:
            pass
        try:
            sh.write("H"+str(row), info["bedrooms"])
        except KeyError:
            pass
        try:
            sh.write("I"+str(row), info["bathrooms"])
        except KeyError:
            pass
        try:
            sh.write("J"+str(row), info["buildingType"])
        except KeyError:
            pass
        try:
            sh.write("K"+str(row), info["propertyLayout"])
        except KeyError:
            pass
        try:
            sh.write("L"+str(row), info["floorZone"])
        except KeyError:
            pass
        try:
            sh.write("M"+str(row), info["contactPerson"])
        except KeyError:
            pass
        try:
            sh.write("N"+str(row), info["contactPhone"])
        except KeyError:
            pass
        if "facilities" in info:
            x = ""
            for i in range(len(info["facilities"])):
                x += info["facilities"][i] + ","
            sh.write("O"+str(row), x)
        if "rooms" in info:
            x = ""
            for i in range(len(info["rooms"])):
                x += info["rooms"][i] + ","
            sh.write("P"+str(row), x)
        if "views" in info:
            x = ""
            for i in range(len(info["views"])):
                x += info["views"][i] + ","
            sh.write("Q"+str(row), x)
        row += 1
    wb.close()

    # Break Lease Appartments #

    # phase 1
    page = 1
    properties = {}
    while True:
        ui.breakLeasePhase1 = "collecting property information...\tpage:{}".format(
            page)
        ui.report()
        url = "https://hongkong.asiaxpat.com/property/break-lease-apartments/{}/".format(
            page)
        lines = general_tools.get_web_text(url)
        j = cover_extractor.extract(lines)
        if len(j) == 0:
            break
        for key in j:
            properties[key] = j[key]
        page += 1
    ui.breakLeasePhase1 = "Done."
    ui.report()

    # phase 2
    step = 0
    dellist = []
    for property in properties:
        step += 1
        ui.breakLeasePhase2 = "collecting details of properties...\t{}/{}".format(
            step, len(properties))
        ui.report()
        info = properties[property]
        url = webhost + info["url"]
        while True:
            lines = general_tools.get_web_text(url)
            j = content_extractor.extract(lines)
            if len(j) > 0:
                break
        for key in j:
            info[key] = j[key]
        if info["subtitle"] == "":
            dellist.append(property)
            continue
        properties[property] = info
    for property in dellist:
        del properties[property]
    ui.breakLeasePhase2 = "Done."
    ui.report()

    # phase 3
    step = 0
    for property in properties:
        step += 1
        image_urls = properties[property]["image_urls"]
        id = properties[property]["id"]
        image = 0
        for image_url in image_urls:
            image += 1
            ui.breakLeasePhase3 = "downloading images of properties... | {}/{} | {}/{}".format(
                step, len(properties), image, len(image_urls))
            ui.report()
            url = webhost + image_url
            general_tools.get_web_photo(
                url, folder+"/image/"+id+"-"+str(image)+".jpg")
    ui.breakLeasePhase3 = "Done."
    ui.report()

    # file export
    wb = xw.Workbook(folder+"/break_lease_apartments.xlsx")
    sh = wb.add_worksheet("output")
    headers = {
        "id": "A1",
        "title": "B1",
        "subtitle": "C1",
        "price": "D1",
        "grossArea": "E1",
        "saleArea": "F1",
        "address": "G1",
        "bedrooms": "H1",
        "bathrooms": "I1",
        "buildingType": "J1",
        "propertyLayout": "K1",
        "floorZone": "L1",
        "contactPerson": "M1",
        "contactPhone": "N1",
        "facilities": "O1",
        "rooms": "P1",
        "views": "Q1"
    }
    for header in headers:
        sh.write(headers[header], header)
    row = 2
    for property in properties:
        info = properties[property]
        try:
            sh.write("A"+str(row), info["id"])
        except KeyError:
            pass
        try:
            sh.write("B"+str(row), info["title"])
        except KeyError:
            pass
        try:
            sh.write("C"+str(row), info["subtitle"])
        except KeyError:
            pass
        try:
            sh.write("D"+str(row), info["price"])
        except KeyError:
            pass
        try:
            sh.write("E"+str(row), info["grossArea"])
        except KeyError:
            pass
        try:
            sh.write("F"+str(row), info["saleArea"])
        except KeyError:
            pass
        try:
            sh.write("G"+str(row), info["address"])
        except KeyError:
            pass
        try:
            sh.write("H"+str(row), info["bedrooms"])
        except KeyError:
            pass
        try:
            sh.write("I"+str(row), info["bathrooms"])
        except KeyError:
            pass
        try:
            sh.write("J"+str(row), info["buildingType"])
        except KeyError:
            pass
        try:
            sh.write("K"+str(row), info["propertyLayout"])
        except KeyError:
            pass
        try:
            sh.write("L"+str(row), info["floorZone"])
        except KeyError:
            pass
        try:
            sh.write("M"+str(row), info["contactPerson"])
        except KeyError:
            pass
        try:
            sh.write("N"+str(row), info["contactPhone"])
        except KeyError:
            pass
        if "facilities" in info:
            x = ""
            for i in range(len(info["facilities"])):
                x += info["facilities"][i] + ","
            sh.write("O"+str(row), x)
        if "rooms" in info:
            x = ""
            for i in range(len(info["rooms"])):
                x += info["rooms"][i] + ","
            sh.write("P"+str(row), x)
        if "views" in info:
            x = ""
            for i in range(len(info["views"])):
                x += info["views"][i] + ","
            sh.write("Q"+str(row), x)
        row += 1
    wb.close()
