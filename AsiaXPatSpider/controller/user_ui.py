import os

class UI:

    def __init__(self):
        self.directOwnerRentPhase1 = "waiting..."
        self.directOwnerRentPhase2 = "waiting..."
        self.directOwnerRentPhase3 = "waiting..."
        self.directOwnerSalePhase1 = "waiting..."
        self.directOwnerSalePhase2 = "waiting..."
        self.directOwnerSalePhase3 = "waiting..."
        self.breakLeasePhase1 = "waiting..."
        self.breakLeasePhase2 = "waiting..."
        self.breakLeasePhase3 = "waiting..."
    
    def report(self):
        os.system("cls")
        print()
        print("$$ Asia XPat Spider $$")
        print("======================")
        print()
        print("Direct Owner Appartments for Rent:")
        print("phase 1: {}".format(self.directOwnerRentPhase1))
        print("phase 2: {}".format(self.directOwnerRentPhase2))
        print("phase 3: {}".format(self.directOwnerRentPhase3))
        print()
        print("Direct Owner Appartments for Sales:")
        print("phase 1: {}".format(self.directOwnerSalePhase1))
        print("phase 2: {}".format(self.directOwnerSalePhase2))
        print("phase 3: {}".format(self.directOwnerSalePhase3))
        print()
        print("Break Lease Apartments:")
        print("phase 1: {}".format(self.breakLeasePhase1))
        print("phase 2: {}".format(self.breakLeasePhase2))
        print("phase 3: {}".format(self.breakLeasePhase3))
        print()
        print("======================")