
from AsiaXPatSpider import general_tools


def extract(lines: list) -> dict:
    """
    Extract Cover Page
    """
    results = {}
    for i in range(len(lines)):
        line = lines[i]
        if line == "Direct Owner" or line == "For Rent": # this two key words mean a new property start from here.
            
            result = {
                "title":"",
                "url":"",
                "image_urls":[]
            }

            i2 = i + 1 # scan from next line
            while i2 < len(lines) and not lines[i2] == "Direct Owner" and not lines[i2] == "For Rent": # before next property, keep checking...

                # [ is the keyword to title information
                if len(lines[i2]) > 0 and lines[i2][0] == "[" and result["title"] == "":
                    # if we found the title line, we join the text before next line break
                    i3 = i2
                    joinText = ""
                    while not lines[i3] == "":
                        joinText += lines[i3]
                        i3 += 1
                    # so, we got the joined text of title information
                    # we extract the info then...
                    result["title"] = joinText.split("]")[0][1:]
                    result["url"] = joinText.split("](")[1].split(" ")[0]

                # start by * and contains keywords "image" means we found the image information
                elif len(lines[i2]) > 0 and "* [![" in lines[i2] and "image" in lines[i2]:
                    url = lines[i2].split("(")[1].split(")")[0] # url is between ( )
                    result["image_urls"].append(url)
                
                # loop for one property
                i2 += 1

            # record result of property to results collection
            url = result["url"]
            id = url.split("/")[-3]
            result["id"] = id
            results[result["title"]] = result
    
    return results        